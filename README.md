# README #

Información útil del backend.

## Datos del servidor ##

* IP: http://46.101.215.26/
* URL: http://fiveapp.tk
* Hosting: DigitalOcean


## Tecnologías empleadas ##

* Node.js (para la aplicación del backend
* Express (módulo de Node.js para correr el servidor)
* Mongod (la base de datos)
* Mongoose (módulo de Node.js para conectar con Mongod)
* Nginx (para hospedar la web estática)

##  Instrucciones ##

La app está corriendo contínuamente mediante un módulo llamado 'forever'. Para ejecutar la app ir al directorio /root/chat y ejecutar:
   
```
#!bash

forever start app.js
```

Para ver el status ejecutar:
   
```
#!bash

forever list
```

Para parar la aplicación ejecutar:
   
```
#!bash

forever stop 0
```

##  Cómo operar con la BBDD ##

La base de datos empleada es MongoDB. Para acceder a ella ejecutar en el terminal:


```
#!bash

mongo
```


Para ver la lista de bases de datos creadas ejecutar:


```
#!bash

show dbs
```


Para acceder a una de las bases de datos del listado anterior ejecutar:


```
#!bash

use nombre-de-la-bb-dd
```


Para mostrar las colecciones (en mongoDB las tablas se llaman colecciones) de esa base de datos ejecutar:


```
#!bash

show collections
```


Para mostrar todo el contenido de una de las colecciones del listado anterior ejecutar:


```
#!bash

db.nombre-de-la-coleccion.find().pretty()
```


También se puede buscar por filtros. Más info de comandos básicos: [http://funnyfrontend.com/instalar-mongodb-y-uso-de-comandos-basicos/](Link URL)


## Funciones implementadas en app.js ##

### **/messages** ###

**URL:** http://fiveapp.tk:3000/messages

**Tipo:** POST

**Autenticación requerida?** NO

**Descripción:** recibe un mensaje de chat y lo guarda en la colección 'History'


### **/signUp** ###

**URL:** http://fiveapp.tk:3000/signUp

**Tipo:** POST

**Autenticación requerida?** NO

**Descripción:** registra un nuevo usuario y lo almacena en la colección 'Users'


### **/login** ###

**URL:** http://fiveapp.tk:3000/login

**Tipo:** POST

**Autenticación requerida?** NO

**Descripción:** loguea un usuario y se le devuelve un token para autenticarse en futuras peticiones



### **/newFriend** ###

**URL:** http://fiveapp.tk:3000/newFriend

**Tipo:** POST

**Autenticación requerida?** SÍ

**Descripción:** añade un nuevo amigo en el listado de amigos del usuario que realiza la petición



### **/listFriends** ###

**URL:** http://fiveapp.tk:3000/listFriends

**Tipo:** GET

**Autenticación requerida?** SÍ

**Descripción:** lista todos los amigos del usuario que realiza la petición



### **/removeFriend** ###

**URL:** http://fiveapp.tk:3000/removeFriend

**Tipo:** POST

**Autenticación requerida?** SÍ

**Descripción:** elimina un amigo de la lista de amigos del usuario que realiza la petición



### **/getName/<username>** ###

**URL:** http://fiveapp.tk:3000/getName/<username>

**Tipo:** GET

**Autenticación requerida?** SÍ

**Descripción:** obtiene el Nombre a partir de un username
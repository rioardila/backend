var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function() {

var historySchema = new Schema({
    text: {type: String, required: true},
    name: {type: String, required: true},
    time: {type: Date, default: Date.now }
  });

  mongoose.model('History', historySchema, 'History');
};

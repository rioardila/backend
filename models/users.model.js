var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = function() {

var userSchema = new Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    name: {type: String, required: true},
    age: {type: Number, required: true},
    img: {mime: String, bin: Buffer},
    friends: [{type : String}]
  });

  mongoose.model('User', userSchema, 'User');
};

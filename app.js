
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var models = require('./models');
var Pusher = require('pusher');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var express_jwt = require('express-jwt');

const saltRounds = 10;

var pusher = new Pusher({
  appId: '224542',
  key: '3a4a3b53b8b8e1af0761',
  secret: 'de4fe57f7c584fc9c328',
  cluster: 'eu',
  encrypted: true
});

var secret = require('./config').jwt_secret;

var express = require('express')
  , routes = require('./routes');

// Nos conectamos a la bd con mongoose
mongoose.connect('mongodb://localhost/appfive');

// Inicializamos los modelos
models.initialize();

var History = require('mongoose').model('History');
var User = require('mongoose').model('User');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));

  //HANDLE UNAUTHORIZED ACCESS (NO HEADER TOKEN)
  app.use(function (err, req, res, next) {
    if(err.name === 'UnauthorizedError') {
      res.status(401).send('Invalid token');
    }
  });
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});


// Routes

//app.get('/', routes.index);

app.post('/messages', function(req, res){
  var message = req.body;
  pusher.trigger('messages', 'new_message', message);
//  res.json({success: 200});
  console.log("New message!");
  console.log(message.text);
  console.log(message.name);
  console.log(message.time);
  console.log("-------------------------");

  //INSERT TO DDBB
  var new_entry = new History(message);
  new_entry.save(function(err, saved) {
    if(!err) {
      console.log("Saved into DDBB");
      res.status(200).json(saved);
    } else {
      console.log(err);
    }
  })
});

//USER REGISTRATION
app.post('/signup', function(req, res, next) {
  //Comprobamos si el usuario ya existe
  User.findOne({username: req.body.username}, function(error,user) {
    //si el usuario no existe lo damos de alta
    if (!user) {
      var userData = req.body;
      //encrypt password
      bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
        // Store hash in DB
        userData.password = hash;
        var new_user = new User(userData);
        new_user.save(function(err, saved) {
          if(!err) {
            console.log("User " + userData.username + " registrered successfully");
            //res.status(200).send('Login correcto');
            var token = jwt.sign(new_user.toObject(), secret);
            //if password is correct, we send the token
            res.json({
              token: token
            });
          } else {
            console.log(err);
          }
        })
      });
    }
    else res.status(401).send("User already exists");
  });
});

//USER LOGIN
app.post('/login', function(req, res, next) {
  //search user in the DDBB
  User.findOne({username: req.body.username}, function(error,user) {
    if (!user) res.status(401).send('User or password invalid');
    else {
      //Check if password is correct
      bcrypt.compare(req.body.password, user.password, function(err, res2) {
        if(!res2) res.status(401).send('User or password invalid');
        else {
          //res.status(200).send('Login correcto');
          var token = jwt.sign(user.toObject(), secret);
          //if password is correct, we send the token
          res.json({
            token: token
          });
        }
      });
    }
  });
});

//ADD NEW FRIEND
app.post('/newFriend', express_jwt({secret: secret}), function(req, res, next) {
  if(req.user.username === req.body.username) {
    res.status(401).send('You cannot add yourself as a friend');
  }
  else {
    //search friend in DDBB
    User.findOne({username: req.body.username}, function(error,friend) {
      if (!friend) res.status(401).send('Friend not found');
      else {
        //only update if user exists and friend is not in friends list
        User.update({$and: [{username: req.user.username}, {friends: {$nin: [friend.username]}}]}, {$push: {friends: friend.username}}, function(err) {
          if(!err) {
            res.status(200).send(req.user.username + ' added ' + req.body.username + ' as a friend');
            console.log(req.user.username + ' added ' + req.body.username + ' as a friend');
          }
        });
      }
    });
  }
});

//RETURN FRIENDS LIST
app.get('/listFriends', express_jwt({secret: secret}), function(req, res, next){
  //search user in DDBB
  User.findOne({username: req.user.username}, function(err, user) {
    if(!user) res.status(401).send('User not found');
    else {
      var list = user.friends;
      res.status(200).json(list);
    }
  })
});

//RETURN NAME OF A USER
app.get('/getName/:user_id', express_jwt({secret: secret}), function(req, res, next){
  //search user in DDBB
  User.findOne({username: req.params.user_id}, function(err,user) {
    if(!user) res.status(401).send('User not found');
    else {
      res.status(200).json(user.name);
    }
  })
});

//REMOVE FRIEND
app.post('/removeFriend', express_jwt({secret: secret}), function(req, res, next) {
  //search logged user in DDBB and remove friend from the list
  User.update({username: req.user.username}, {$pull: {friends: {$in: [req.body.username]}}}, function(err, affected) {
    //affected.nModified counts the number of updates
    if(!affected.nModified) res.status(401).send('Friend not found');
    else {
      if(!err) {
        res.status(200).send(req.user.username + ' removed ' + req.body.username + ' as a friend');
        console.log(req.user.username + ' removed ' + req.body.username + ' as a friend');
      }
    }
  });
});

//404 ERROR DEFUALT ROUTE
app.use(function(req,res) {
  res.send(404);
});


app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
